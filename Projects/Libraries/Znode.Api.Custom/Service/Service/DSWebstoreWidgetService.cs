﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.Service.Service
{
    public class DSWebstoreWidgetService : WebStoreWidgetService
    {       
        private readonly IPublishProductService _publishProductService;
        private readonly IZnodeRepository<ZnodeFormBuilder> _formBuilderRepository;
        private readonly IZnodeRepository<ZnodeCMSFormWidgetConfiguration> _formWidgetConfigurationRepository;
        private readonly IZnodeRepository<ZnodePublishProduct> _publishProductRepository;
        private readonly IZnodeRepository<ZnodePimLinkProductDetail> _linkDetailRepository;
        private readonly IZnodeRepository<ZnodeMedia> _mediaDetails;
        private readonly IPublishedPortalDataService publishedPortalDataService;

        public DSWebstoreWidgetService() : base()
        {           
            _formBuilderRepository = new ZnodeRepository<ZnodeFormBuilder>();
            _publishProductRepository = new ZnodeRepository<ZnodePublishProduct>();
            _linkDetailRepository = new ZnodeRepository<ZnodePimLinkProductDetail>();
            _mediaDetails = new ZnodeRepository<ZnodeMedia>();
            publishedPortalDataService = ZnodeDependencyResolver.GetService<IPublishedPortalDataService>();
            _formWidgetConfigurationRepository = new ZnodeRepository<ZnodeCMSFormWidgetConfiguration>();
        }

        #region Base Methods Override 9.5

        public override WebStoreLinkProductListModel GetLinkProductList(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        {
            WebStoreLinkProductListModel webStoreLinkProductListModel = new WebStoreLinkProductListModel();
            webStoreLinkProductListModel = base.GetLinkProductList(parameter, expands);
            webStoreLinkProductListModel = GetSwatchImagesForLinkProductsOnPDP(webStoreLinkProductListModel, parameter.PortalId);
            return webStoreLinkProductListModel;
        }

        private WebStoreLinkProductListModel GetSwatchImagesForLinkProductsOnPDP(WebStoreLinkProductListModel webStoreLinkProductListModel, int portalId)
        {

            DSAttributeSwatchHelper dsAttributeSwatchHelper = new Helper.DSAttributeSwatchHelper();
            int? _linkproductcount = webStoreLinkProductListModel.LinkProductList?.Count;
            for (int i = 0; i < _linkproductcount; i++)
            {
                dsAttributeSwatchHelper.GetpublishedConfigurableProducts(webStoreLinkProductListModel.LinkProductList[i].PublishProduct, portalId, "drewcolor");
            }
            return webStoreLinkProductListModel;

        }

     

        private List<PublishProductModel> AssignSwatchImages(List<PublishProductModel> publishProducts, int portalId)
        {
            DSAttributeSwatchHelper dsAttributeSwatchHelper = new Helper.DSAttributeSwatchHelper();
            dsAttributeSwatchHelper.GetpublishedConfigurableProducts(publishProducts, portalId, "drewcolor");

            return publishProducts;
        }

        private WebStoreWidgetProductListModel ToWebstoreWidgetProductListModel(List<PublishProductModel> model)
        {
            WebStoreWidgetProductListModel webStoreWidgetProductListModel = new WebStoreWidgetProductListModel();
            webStoreWidgetProductListModel.Products = new List<WebStoreWidgetProductModel>();

            foreach (PublishProductModel data in model)
            {
                WebStoreWidgetProductModel webStoreWidgetProductModel = new WebStoreWidgetProductModel();
                webStoreWidgetProductModel.WebStoreProductModel = MapData(data);
                webStoreWidgetProductListModel.Products.Add(webStoreWidgetProductModel);
            }
            return webStoreWidgetProductListModel;
        }

       
        private WebStoreProductModel MapData(PublishProductModel model)
        {
            return new WebStoreProductModel
            {
                Attributes = model.Attributes,
                LocaleId = model.LocaleId,
                CatalogId = model.PublishedCatalogId,
                PublishProductId = model.PublishProductId,
                Name = model.Name,
                RetailPrice = model.RetailPrice,
                SalesPrice = model.SalesPrice,
                PromotionalPrice = model.PromotionalPrice,
                SKU = model.SKU,
                ProductReviews = model.ProductReviews,
                CurrencyCode = model.CurrencyCode,
                ImageMediumPath = model.ImageMediumPath,
                ImageSmallPath = model.ImageSmallPath,
                ImageSmallThumbnailPath = model.ImageSmallThumbnailPath,
                ImageThumbNailPath = model.ImageThumbNailPath,
                ImageLargePath = model.ImageLargePath,
                SEODescription = model.SEODescription,
                SEOTitle = model.SEOTitle,
                Rating = model.Rating,
                TotalReviews = model.TotalReviews,
                SEOUrl = model.SEOUrl,
                SEOKeywords = model.SEOKeywords,
                GroupProductPriceMessage = model.GroupProductPriceMessage,
                Promotions = model.Promotions,
                AlternateImages = model.AlternateImages

            };
        }

        private FilterCollection CreateFilterCollectionForProducts(WebStoreWidgetParameterModel parameter)
      => new FilterCollection() {
                new FilterTuple("MappingId", FilterOperators.Equals, parameter.CMSMappingId.ToString()),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.WidgetsKey.ToString(), FilterOperators.Like, parameter.WidgetKey),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.TypeOFMapping.ToString(), FilterOperators.Like, parameter.TypeOfMapping),
        };

        #endregion

        #region Base Methods Override 9.6

        //Get Product list widget with product details.
        public override WebStoreWidgetProductListModel GetProducts(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);

            PublishProductListModel model = new PublishProductListModel();

            List<ZnodePublishWidgetProductEntity> productListWidgetEntity = publishedPortalDataService.GetProductWidget(GetFilters(parameter));

            int? versionId = GetCatalogVersionId();

            List<ZnodePublishCategoryEntity> categoryListWidgetEntity = ZnodeDependencyResolver.GetService<IPublishedCategoryDataService>().GetCategoryListByCatalogId(parameter.PublishCatalogId, parameter.LocaleId).Where(x => x.VersionId == versionId && x.IsActive).ToList();

            List<string> SKUs = productListWidgetEntity?.OrderBy(a => a.DisplayOrder).Select(x => x.SKU)?.ToList();
            foreach (ZnodePublishCategoryEntity item in categoryListWidgetEntity)
            {
                if ((item.ActivationDate == null || item.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate()) && (item.ExpirationDate == null || item.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate()))
                    parameter.CategoryIds += item.ZnodeCategoryId.ToString() + ",";
            }

            //if (SKUs?.Count > 0)
            //    model = GetPublishProducts(expands, ProductListFilters(parameter, SKUs), null, null);

            //ZnodeLogging.LogMessage("Count of PublishProducts list in PublishProductListModel returned from method GetPublishProducts: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, model?.PublishProducts?.Count);

            //WebStoreWidgetProductListModel listModel = ToWebstoreWidgetProductListModel(model?.PublishProducts, productListWidgetEntity);

            //listModel.DisplayName = parameter.DisplayName;
            //ZnodeLogging.LogMessage("DisplayName property of WebStoreWidgetProductListModel", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, listModel?.DisplayName);
            //ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            //return listModel;

            //nivicode start
            expands = new NameValueCollection();
            if (SKUs?.Count > 0)
            // model = GetPublishProducts(expands, ProductListFilters(parameter, string.Join(",", SKUs)), null, null);
            model = GetPublishProducts(expands, ProductListFilters(parameter, SKUs), null, null);
            if (model.PublishProducts != null && model.PublishProducts.Count > 0)
            {
                model.PublishProducts = AssignSwatchImages(model?.PublishProducts, parameter.PortalId);
            }
            WebStoreWidgetProductListModel listModel = ToWebstoreWidgetProductListModel(model?.PublishProducts);
            ZnodeLogging.LogMessage("DisplayName property of WebStoreWidgetProductListModel", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, listModel?.DisplayName);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            listModel.DisplayName = parameter.DisplayName;
            return listModel;
            //nivicode End
        }
        private FilterCollection GetFilters(WebStoreWidgetParameterModel parameter)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add("MappingId", FilterOperators.Equals, parameter.CMSMappingId.ToString());
            filters.Add("WidgetsKey", FilterOperators.Is, parameter.WidgetKey);
            filters.Add("TypeOFMapping", FilterOperators.Like, parameter.TypeOfMapping);
            filters.Add("VersionId", FilterOperators.Equals, WebstoreVersionId.ToString());
            return filters;
        }
        //Generate filters to render product list.
        private FilterCollection ProductListFilters(WebStoreWidgetParameterModel parameter, List<string> SKUs)
        {
            FilterCollection filter = new FilterCollection();
            if (SKUs?.Count > 0)
                filter.Add(new FilterTuple(WebStoreEnum.SKU.ToString(), FilterOperators.In, string.Join(",", SKUs.Select(x => $"\"{x}\""))));
            filter.Add(new FilterTuple(FilterKeys.LocaleId, FilterOperators.Equals, parameter.LocaleId.ToString()));
            filter.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, parameter.PortalId.ToString()));
            filter.Add(new FilterTuple(ZnodeLocaleEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue));
            filter.Add(FilterKeys.ProductIndex, FilterOperators.Equals, ZnodeConstant.DefaultPublishProductIndex.ToString());
            filter.Add(new FilterTuple(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, parameter.PublishCatalogId.ToString()));
            if (!string.IsNullOrEmpty(parameter.CategoryIds))
                filter.Add(new FilterTuple(FilterKeys.CategoryIds, FilterOperators.In, parameter.CategoryIds?.TrimEnd(',')));
            return filter;
        }
    
        #endregion
    }
}
