﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Service.Service
{
   public class DSTouchPointConfigurationService : TouchPointConfigurationService
    {
        public override bool TriggerTaskScheduler(string connectorTouchPoints)
        {
            try
            {
                ZnodeLogging.LogMessage("CONNECTORTOUCHPOINTS="+ connectorTouchPoints, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);

                //Get current active class name
                IERPConfiguratorService eRPConfiguratorService = ZnodeDependencyResolver.GetService<IERPConfiguratorService>();
                Assembly assembly = Assembly.Load("Znode.Engine.ERPConnector");
                Type className = assembly.GetTypes().FirstOrDefault(g => g.Name == eRPConfiguratorService.GetActiveERPClassName());
                ZnodeLogging.LogMessage("GETACTIVEERPCLASSNAME=" + Convert.ToString(className.Name), ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                //Create Instance of active class
                object instance = Activator.CreateInstance(className);

                //Get Method Information from class
                MethodInfo info = className.GetMethod(connectorTouchPoints);
                ZnodeLogging.LogMessage("METHODINFO=" + info.Name, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                //Calling method with null parameter
                string invokeStatus = Convert.ToString(info.Invoke(instance, null));
                ZnodeLogging.LogMessage("INVOKESTATUS=" + invokeStatus, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return !string.IsNullOrEmpty(invokeStatus) && invokeStatus == "True";
            }
            catch(Exception ex)
            {
                ZnodeLogging.LogMessage("ERROR!! "+ ex.Message, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return false;
            }
        }
    }
}
