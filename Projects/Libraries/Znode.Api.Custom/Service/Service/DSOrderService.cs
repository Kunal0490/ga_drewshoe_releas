﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.Api.Custom.Service.Service
{
    public class DSOrderService : OrderService
    {
        #region private variable
        private string url = Convert.ToString(ConfigurationManager.AppSettings["DrewEverestApiUrl"]);
        private string key = Convert.ToString(ConfigurationManager.AppSettings["DrewEverestApiKey"]);
        private int RequestTimeout { get; set; } = 10000000;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _omsOrderDetailRepository;
        #endregion

        #region Constructor
        public DSOrderService() : base()
        {
            _omsOrderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();

        }
        #endregion Constructor

     

        #region public methods

        public override OrderModel CreateOrder(ShoppingCartModel model)
        {
            OrderModel orderModel = base.CreateOrder(model);

            /*
             * EVEREST CREATE ORDER SECTION BEGINS
             1.  Call Address Creation API and pass AddressModel to it get generated ShipToCode from it.
             2.  Call Order Creation API and pass Ordermodel to it and get generated Everest Order Number from it.
             3. If order Success Save EverestOrderNumber in some field
             4. If fail then?
             */
            try
            {

                orderModel.AdditionalInstructions = model.AdditionalInstructions;
                if (model.ShippingAddress.EmailAddress == "" || model.ShippingAddress.EmailAddress == null)
                    model.ShippingAddress.EmailAddress = orderModel.ShippingAddress.EmailAddress;
                string ShipToCode = CreateAddressInEverest(model.ShippingAddress);
                // Log("ShipToCode=" + ShipToCode);
                model.ShoppingCartItems.ForEach(y =>
                orderModel.OrderLineItems.Where(x => x.Sku == y.ConfigurableProductSKUs).FirstOrDefault().Custom1 = y.ProductCode
                );
                if (orderModel.CouponCode == null)
                {
                    orderModel.CouponCode = model.Shipping.ShippingDiscountDescription;
                }
                if (model.Coupons?.Count > 0)
                {
                    orderModel.CouponCode = model.Coupons[0].Code;
                }
                orderModel.Custom2 = ShipToCode;
                int everestOrderNo = CreateOrderInEverest(orderModel);
                // Log("everestOrderNo=" + Convert.ToString(everestOrderNo));
                /*Everest Order Number need to be saved in our system if Custom1 is being used for some other
                 Purpose then set everestOrderNo to some other field */
                ZnodeOmsOrderDetail createdOrder = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderId == orderModel.OmsOrderId).FirstOrDefault();
                createdOrder.Custom1 = Convert.ToString(everestOrderNo);
                createdOrder.Custom2 = ShipToCode;
                if (everestOrderNo == 0)
                    createdOrder.OmsOrderStateId = 140;//FAILED
                _omsOrderDetailRepository.Update(createdOrder);
                ZnodeLogging.LogMessage("Create Order Method ShipToCode=: " + ShipToCode + " everestOrderNo: " + Convert.ToString(everestOrderNo)+ "ZnodeOrder Number: " + orderModel.OrderNumber, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                //  Log("Order Updated");
            }
            catch (Exception ex)
            {
                Log("ERROR!!-" + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace);
                ZnodeLogging.LogMessage("Unable to Create Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                ZnodeOmsOrderDetail createdOrder = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderId == orderModel.OmsOrderId).FirstOrDefault();
               createdOrder.OmsOrderStateId = 140;//FAILED
               _omsOrderDetailRepository.Update(createdOrder);
            }
            return orderModel;
        }

        public override OrderModel UpdateOrder(OrderModel model)
        {

            OrderModel modifiedModel = base.UpdateOrder(model);
            try
            {
                bool isOrderUpdatedInEverest = false;
                ZnodeLogging.LogMessage("UPDATEORDER: model.OrderState=" + model.OrderState, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                if (model.OrderState.ToUpper() == "CANCELLED")
                {
                    isOrderUpdatedInEverest = CancelOrderinEverest(model);
                }
                else
                {
                    isOrderUpdatedInEverest = UpdateOrderinEverest(model);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Unable to Update Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }

            return modifiedModel;
        }
        #endregion

        #region Everest
        #region Address Creation    
        private string CreateAddressInEverest(AddressModel address)
        {
            string ShipToCode = "";
            try
            {
                string addressurl = url + "CreateAddress";
                //string json = JsonConvert.SerializeObject(address);
                if (address.CountryName == "US")
                    address.CountryName = "United States";
                string result = GetEverestResponse(addressurl, key, JsonConvert.SerializeObject(address));


                if (result.Contains("Error"))
                {
                    ZnodeLogging.LogMessage("Unable to Create Address In Everest ErrorDetails:" + result, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                }
                else
                {
                    string firstString = "<AddressCode>";
                    string lastString = "</AddressCode>";
                    int pos1 = result.IndexOf(firstString) + firstString.Length;
                    int pos2 = result.Substring(pos1).IndexOf(lastString);
                    ShipToCode = result.Substring(pos1, pos2);

                }
                return ShipToCode;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Unable to Create Address In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return ShipToCode;
            }
        }
        #endregion

        #region Order Section
        private int CreateOrderInEverest(OrderModel model)
        {
            int everestOrderNumber = 0;
            try
            {
                string orderCreationurl = url + "/CreateSaleOrder";
                // string json = JsonConvert.SerializeObject(model);
                string result = GetEverestResponse(orderCreationurl, key, JsonConvert.SerializeObject(model));
                if (result.Contains("Error"))
                {
                    ZnodeLogging.LogMessage("Unable to Create Order In Everest ErrorDetails:" + result, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                }
                else
                {
                    string firstString = "<DocumentNo>";
                    string lastString = "</DocumentNo>";
                    int pos1 = result.IndexOf(firstString) + firstString.Length;
                    int pos2 = result.Substring(pos1).IndexOf(lastString);
                    everestOrderNumber = Convert.ToInt32(result.Substring(pos1, pos2));

                }

                return everestOrderNumber;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Unable to Create Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return everestOrderNumber;
            }

        }

        private bool UpdateOrderinEverest(OrderModel model)
        {
            bool issuccess = false;
            try
            {
                string orderCreationurl = url + "/UpdateSaleOrder";
                // string json = JsonConvert.SerializeObject(model);
                string result = GetEverestResponse(orderCreationurl, key, JsonConvert.SerializeObject(model));

                if (result.Contains("Error"))
                {
                    ZnodeLogging.LogMessage("Unable to Update Order In Everest ErrorDetails:" + result, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                }
                else
                {
                    issuccess = true;
                }
                return issuccess;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Unable to Update Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return issuccess;
            }

        }

        private bool CancelOrderinEverest(OrderModel model)
        {
            bool issuccess = false;
            try
            {
                string orderCreationurl = url + "/CancelSaleOrder";
                // string json = JsonConvert.SerializeObject(model);
                string result = GetEverestResponse(orderCreationurl, key, JsonConvert.SerializeObject(model));

                if (result.Contains("Error"))
                {
                    ZnodeLogging.LogMessage("Unable to Cancel Order In Everest ErrorDetails:" + result, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                }
                else
                {
                    issuccess = true;
                }
                return issuccess;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Unable to Cancel Order In Everest ErrorDetails: " + ex.Message + " Inner Exception: " + ex.InnerException + " StackTrace: " + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return issuccess;
            }

        }
        #endregion

        #region response      

        private string GetEverestResponse(string url, string key, string data)
        {

            byte[] dataBytes = Encoding.UTF8.GetBytes(data);

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "POST";
            req.ContentType = "application/json";
            req.ContentLength = dataBytes.Length;
            req.Timeout = RequestTimeout;

            string authoriseHeader = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(url + "|" + key));
            req.Headers.Add(HttpRequestHeader.Authorization, "Basic " + authoriseHeader);

            using (Stream reqStream = req.GetRequestStream())
            {
                reqStream.Write(dataBytes, 0, dataBytes.Length);
            }
            HttpWebResponse response = (HttpWebResponse)req.GetResponse();
            string result = "";
            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");

            using (var webResponse = req.GetResponse())
            {
                Stream responseStream = webResponse.GetResponseStream();
                StreamReader readStream = new StreamReader(responseStream, encode);
                Char[] read = new Char[256];

                int count = readStream.Read(read, 0, 256);
                while (count > 0)
                {

                    String str = new String(read, 0, count);
                    result = result + str;
                    count = readStream.Read(read, 0, 256);
                }
            }

            response.Close();
            return result;
        }
        #endregion

        public string Log(string logMessage)
        {
            string logfilepath = ConfigurationManager.AppSettings["FIlelogPath"];
            try
            {
                using (StreamWriter writer = File.AppendText(logfilepath + "\\" + "log.txt"))
                {
                    // Log(logMessage, w);

                    writer.Write("\r\nLog Entry : ");
                    writer.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                    writer.WriteLine("  :");
                    writer.WriteLine("  :{0}", logMessage);
                    writer.WriteLine("-------------------------------");
                }
            }
            catch (Exception ex)
            {
                return "Error : " + ex.Message + " StackTrace: " + ex.StackTrace;
            }
            return string.Empty;
        }

        #endregion
    }
}
