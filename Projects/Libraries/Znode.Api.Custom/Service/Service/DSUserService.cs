﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Service.Service
{
    public class DSUserService: UserService
    {
        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private readonly IZnodeRepository<ZnodeUserProfile> _accountProfileRepository;
        private readonly IZnodeRepository<ZnodeUserPortal> _userPortalRepository;
        private readonly IZnodeRepository<ZnodePortalProfile> _portalProfileRepository;
        private readonly IZnodeRepository<ZnodeProfile> _profileRepository;
        private readonly IZnodeRepository<ZnodeDepartmentUser> _departmentUserRepository;
        private readonly IZnodeRepository<ZnodeAccountProfile> _accountAssociatedProfileRepository;
        private readonly IZnodeRepository<ZnodeAccountPermissionAccess> _accountPermissionAccessRepository;
        private readonly IZnodeRepository<ZnodeAccountPermission> _accountPermissionRepository;

        string ResponseAPIKey = ConfigurationManager.AppSettings["GetResponseAPIKey"];
        string ResAPIUrl = ConfigurationManager.AppSettings["GetResponseAPIURL"];
        string CampaignName = ConfigurationManager.AppSettings["CampaignName"];
        #region Constructor

        public DSUserService() : base()
        {
            _userRepository = new ZnodeRepository<ZnodeUser>();
            _accountProfileRepository = new ZnodeRepository<ZnodeUserProfile>();
            _userPortalRepository = new ZnodeRepository<ZnodeUserPortal>();
            _portalProfileRepository = new ZnodeRepository<ZnodePortalProfile>();
            _profileRepository = new ZnodeRepository<ZnodeProfile>();
            _departmentUserRepository = new ZnodeRepository<ZnodeDepartmentUser>();
            _accountAssociatedProfileRepository = new ZnodeRepository<ZnodeAccountProfile>();
            _accountPermissionAccessRepository = new ZnodeRepository<ZnodeAccountPermissionAccess>();
            _accountPermissionRepository = new ZnodeRepository<ZnodeAccountPermission>();
        }

        #endregion Constructor


        public override bool SignUpForNewsLetter(NewsLetterSignUpModel model)
        {
            bool Isvalid = base.SignUpForNewsLetter(model);
            if (Isvalid)
            {
                try
                {
                    //GetNewsLetterCampaign(model.Email);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, "DSUserService:SignUpForNewsLetter ", TraceLevel.Error);
                }
            }
            return Isvalid;
        }

        public void GetNewsLetterCampaign(string Email)
        {
           
        }

    }
}
