﻿using Autofac;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.Engine.WebStore
{
    public class CustomDependancyRegistration : IDependencyRegistration
    {
        public virtual void Register(ContainerBuilder builder)
        {
            //builder.RegisterType<CustomUserController>().As<UserController>().InstancePerDependency();
            builder.RegisterType<DSCustomAgent>().As<IDSCustomAgent>().InstancePerLifetimeScope();
            builder.RegisterType<DSCartAgent>().As<ICartAgent>().InstancePerDependency();
            builder.RegisterType<DSWidgetDataAgent>().As<IWidgetDataAgent>().InstancePerDependency();
        }
        public int Order
        {
            get { return 1; }
        }
    }
}